<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<style type="text/css">	
#dt_basic_length{float:right;}
#dt_basic_wrapper .DTTT.btn-group{float:right; margin-left:10px;  margin-right:10px;}
.smart-style-5 div.DTTT .btn{margin:0px;}
@media only screen and (min-width : 480px)
{
#dt_basic_filter{float: left;width: 50%;}
}

.loader {
  width: 3em;
  height: 3em;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border-radius: 50%;
  box-shadow: 0 0 5px #444;
 
}
.loader:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: linear-gradient(#009EE3 , black 90%);
  animation: spin .5s infinite linear;
}
.loader:after {
  content: "";
  position: absolute;
  width: 90%;
  height: 90%;
  top: 5%;
  left:5%;
  background-color:#fff;
  border-radius: 50%;
  box-shadow: inset 0 0 5px #444;
}

@keyframes spin {
  to {
    transform: rotate(360deg);
  }
}
.load-bg{
    position:absolute;
    top:0;
    left:0;   
    width:100%;
    height:100%;
    
    z-index:99999;
    display:block;
}
.loader1{position:relative; float:left; min-height:200px; width:100%;} 
</style>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
  $(document).ready(function() {
	
	$("#errorTariffClass").html("");
	
	var tariffClassName = new Array();
	<?php if(isset($arrTariffClassListInfo[0]['trffclass']) && $arrTariffClassListInfo[0]['trffclass']!=''){ ?> 		
    <?php foreach($arrTariffClassListInfo as $arrResult){ ?>
        tariffClassName.push('<?php echo $arrResult['trffclass']; ?>');
    <?php } ?>
	<?php } ?>   
    $("#tags").autocomplete({
      source: tariffClassName,
	  response: function(event, ui) {        		
            if (ui.content.length === 0) {
                $("#errorTariffClass").html("Tariff Class not found");               				
            }else{
				$("#errorTariffClass").html("");
			}           
        }
    });
	
	$('#tags').on('autocompleteselect', function (e, ui) {
        //$('#tagsname').html('You selected: ' + ui.item.value);
        //alert(ui.item.value);
		var selectTariffClass = ui.item.value;
		var siteCode = $("#siteCode").val();
		var mobileNo = $("#mobileNo").val();		
		if(selectTariffClass!=''){
				  //alert(selectTariffClass);
				  $("#tickMark").css("display", "block");
				  $('#tickMark').html('<div class="load-bg"><div class="loader"></div></div>');
				  
				  var dataString = 'selectTariffClass='+selectTariffClass+'&siteCode='+siteCode+'&mobileNo='+mobileNo;		
				  $.ajax({
					   type: "POST",
					   url: "<?php echo base_url(); ?>tariff/updateTariffClass",
					   data: dataString,
					   //dataType: 'json',
					   success: function(data){ 
							//alert(data);
							if(data=="0"){							  
							  $('#tickMark').html('<button type="button" class="btn btn-primary"><i class="fa fa-check line-height18 pd-0"></i></button>');
							  //$("#tickMark").css("display", "block");
							}else{
								$("#errorTariffClass").html("Tariff Class not update");
							}							
					   }
					 }); 			  			  
			  }
        
    });
	
  });
</script>
  
		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!--<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-reset-msg="Would you like to RESET all your saved widgets and clear LocalStorage?"><i class="fa fa-refresh"></i></span> 
				</span>

				 breadcrumb 
				<ol class="breadcrumb"><li>Home</li><li>App Views</li><li>Profile</li></ol>-->
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right" style="margin-right:25px">
					<a href="#" id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa fa-grid"></i> Change Grid</a>
					<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa fa-plus"></i> Add</span>
					<button id="search" class="btn btn-ribbon" data-title="search"><i class="fa fa-search"></i> <span class="hidden-mobile">Search</span></button>
				</span>

			</div> -->
		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Tariff Testing </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
							
							
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span> 
									<h2>SiteCode & Mobile Number</h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<form name="getTariffInfoform" class="form-horizontal" id="getTariffInfoform" method="post" action="<?php echo base_url();?>tariff">
													<div class="col-sm-12 col-md-6 col-md-offset-3">
														<div class="form-group">
															<label class="col-sm-4  line-height32 col-md-3 text-right">SiteCode &nbsp;<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-5">
																<select class="form-control" name="siteCode" id="siteCode">
																	<option value="">Select</option>
																	<?php foreach($arrSiteCodeInfo as $key=>$value){ ?>
																	<option value="<?php echo $value['SiteCode']; ?>" <?php if(isset($varSiteCode) && $varSiteCode!='' && $varSiteCode==$value['SiteCode']){ ?> selected <?php } ?>><?php echo $value['SiteCode']; ?></option>
																	<?php } ?>															
																	
																</select>
															</div>
															<label class="col-sm-4  line-height32 col-md-3 text-right">Number &nbsp;<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-5">
															<input class="form-control" type="text" name="mobileNo" id="mobileNo" value="<?php if(isset($varMobileNo) && $varMobileNo!=''){  echo $varMobileNo; } ?>" maxlength="20" autocomplete="off" placeholder="Enter mobile number" onkeydown="restrictSpace(this.id)" onkeypress="return isNumber(event)" />																
															</div>
															<div class="col-sm-2">
																<button type="submit" class="btn btn-primary">GO</button>
															</div>
														</div>														
													</div>													
												</form>
											</div>
											
										</div>										
									</div>
								</div>
							</div>
							
							
							
							<?php if(isset($varMobileNo)){ ?>
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span> 
									<h2>Master Balance </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding" id="balanceDiv">
										<?php if(isset($arrMainBalanceInfo['current_bal']) && $arrMainBalanceInfo['current_bal']!=''){ ?> 
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Product</th>
													<th data-hide="phone">Current Balance</th>
													<th>Action</th>													
												</tr>
											</thead>
											<tbody>												
												<tr>
													<td><?php echo $arrMainBalanceInfo['product_type'];?></td>
													<td><?php echo $arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode'];?></td>
													<td>
														<a href="#" onclick="return addModelFunction();" id="addModel" data-toggle="modal" class="btn btn-primary" data-target="#myModal1">Add</a> 
														<a href="#" onclick="return removeModelFunction();" id="removeModel" data-toggle="modal" class="btn btn-danger" data-target="#myModal2">Remove</a>
													</td>
												</tr>											
																							
											</tbody>
										</table>
										<?php }else{ ?>	
										<br><br><b><center> No records found</center></b>
										<?php } ?>
									</div>
								</div>
							</div>
							
							
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span>
									<h2>Master Tariff class</h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<?php if(isset($arrMainBalanceInfo['master_tariff']) && $arrMainBalanceInfo['master_tariff']!=''){ ?> 
									<div class="widget-body no-padding">			
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<!--form class="form-horizontal" action=""-->
													<div class="col-sm-12 col-md-6 col-md-offset-3">
														<div class="form-group">
															<label class="col-sm-4  line-height32 col-md-3 text-right" for="tags">Chosen Tariff class &nbsp;<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-5">
																<input type="text" id="tags" class="form-control" value="<?php if(isset($arrMainBalanceInfo['master_tariff']) && $arrMainBalanceInfo['master_tariff']!=''){  echo $arrMainBalanceInfo['master_tariff']; } ?>" maxlength="20" autocomplete="off" />
																<span id="errorTariffClass" style="color:red;"></span>
															</div>
															<div class="col-sm-2">
																<span id="tickMark" style="display:none;"><button type="button" class="btn btn-primary"><i class="fa fa-check line-height18 pd-0"></i></button></span>
															</div>
														</div>														
													</div>													
												<!--/form-->
											</div>
											
										</div>
									
									</div>
									<?php }else{ ?>	
										<br><b><center> No records found</center></b><br><br>
									<?php } ?>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span>
									<h2>Bundles</h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<?php if(isset($arrBundlesInfo[0]['name']) && $arrBundlesInfo[0]['name']!=''){ ?> 
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
												    <th data-class="expand">S.No</th>
													<th data-hide="phone">Package Name</th>
													<th data-hide="phone">Type</th>
													<th data-hide="phone">Product</th>
													<th data-hide="phone">BundleId</th>
													<th data-hide="phone">PackageId</th>
													<th data-hide="phone,tablet">Tariff Class</th>
													<th data-hide="phone,tablet">Start Date</th>
													<th data-hide="phone,tablet">Expiry Date</th>
													<th data-hide="phone,tablet">Status</th>
													<th data-hide="phone,tablet">Initial Balance</th>
													<th data-hide="phone,tablet">Current Balance</th>
													<th>Action</th>
													
												</tr>
											</thead>
											<tbody>
											<?php $i=1; foreach($arrBundlesInfo as $arrResult){  ?>	
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $arrResult['name']; ?></td>
													<td><?php echo $arrResult['package_types']; ?></td>
													<td><?php echo $arrResult['product']; ?></td>
													<td><?php echo $arrResult['bundleid']; ?></td>
													<td><?php echo $arrResult['package_id']; ?></td>
													<td><?php echo $arrResult['tariffClass']; ?></td>
													<td><?php echo $arrResult['startDate']; ?></td>
													<td><?php echo $arrResult['expDate']; ?></td>
													<td><?php echo $arrResult['pack_status']; ?></td>
													<td><?php echo $arrResult['initial_balance']; ?></td>
													<td><?php echo $arrResult['current_balance']; ?></td>
													<td>
														<a href="#" data-toggle="modal" class="btn btn-primary" data-target="#myModal3">Edit</a>
													</td>
												</tr>
												<?php $i++; } ?>	
												<!--tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
													<td>UK</td>
													<td>UK</td>
													<td>UK</td>
													<td>
														<a href="#" data-toggle="modal" class="btn btn-primary" data-target="#myModal2">Edit</a>
													</td>
												</tr-->												
											</tbody>
										</table>
										<?php }else{ ?>	
										<br><br><b><center> No records found</center></b>
										<?php } ?>
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							<?php } ?>
							
				<!-- modal-->
				 <div class="modal fade" id="myModal1" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Add balance</h4>
							</div>
							<div class="modal-body">								
								<div class="row">
									<form name="addCreditform" class="form-horizontal" id="addCreditform" method="post" action="<?php echo base_url();?>tariff/updateAddBalance">
									  <input type="hidden" name="currency" id="currency" value="<?php echo $arrMainBalanceInfo['currcode'];?>" />
										<div class="form-group">
											<b><label class="col-sm-4 control-label">Current Balance</label></b>
											<div class="col-sm-6">
												<b><p class="form-control-static"><?php echo $arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode'];?></p></b>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Add Amount</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control amount" maxlength="20" placeholder="Enter amount" name="addAmount" id="addAmount" value="" autocomplete="off" />
											</div>
											<div class="col-sm-2 col-xs-3">
												<p class="form-control-static">GBP</p>
											</div>
										</div>										
										<div class="form-group">
											<label class="col-sm-4 control-label">Mode</label>
											<div class="col-sm-12 col-xs-12">
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" name="modeValue" value="1" /> Web top-up
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" name="modeValue" value="2" /> E-Voucher
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" name="modeValue" value="3" /> Physical Voucher
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" name="modeValue" value="4" /> Manual
												</label>
											</div>
											</div>
										</div>
										<div class="form-group m-t-20" id="paymentReferDiv" style="display:none;">
											<label class="col-sm-4 control-label">Payment Reference</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" name="paymentRefer" id="paymentRefer" maxlength="20" autocomplete="off" placeholder="Enter payment reference" value="" />
											</div>
										</div>
										<div class="form-group m-t-20" id="voucherDiv" style="display:none;">
											<label class="col-sm-4 control-label">Voucher Number</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" name="vnumber" id="vnumber" maxlength="20" autocomplete="off" placeholder="Enter voucher number" onkeydown="restrictSpace(this.id)" onkeypress="return isNumber(event)" value="" />
											</div>
										</div>
										<div class="form-group m-t-20">
											<label class="col-sm-4 control-label">Comments</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" name="comments" id="comments" maxlength="40" autocomplete="off" placeholder="Enter comments" value="" />
											</div>
										</div>
									
								</div>
								<?php if(isset($arrAddRemoveCreditInfo[0]['CreatedDate']) && $arrAddRemoveCreditInfo[0]['CreatedDate']!=''){ ?> 
								<div class="row m-t-20">
									<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
										<header>
											<h2>Log Reference</h2>
						
										</header>				
										<!-- widget div-->
										<div>					
											<!-- widget edit box -->									
											<!-- end widget edit box -->					
											<!-- widget content -->
												<div class="widget-body no-padding">	
												
												<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
														<tr>
															<!--th data-class="expand">S.No</th-->
															<th data-class="expand">Date</th>
															<th data-hide="phone,tablet">Amount Added</th>
															<th data-hide="phone,tablet">Mode</th>
															<th data-hide="phone,tablet">Comments</th>															
															
														</tr>
													</thead>
													<tbody>
													<?php $i=1; foreach($arrAddRemoveCreditInfo as $arrResult){  ?>	
															<?php if($arrResult['ActionType']=="A"){ ?>
														<tr>
															<!--td><?php //echo $i; ?></td-->
															<td><?php echo $arrResult['CreatedDate']; ?></td>
															<td><?php echo $arrResult['Amount']; ?></td>
															<td><?php if($arrResult['Paymode']!=''){ echo $arrResult['Paymode']; }else{ echo '-';} ?></td>
															<td><?php echo $arrResult['Comments']; ?></td>
														</tr>
															<?php } ?>
													<?php $i++; } ?>			
													</tbody>
												</table>
											</div>
										</div>
									</div>									
								</div>	
								<?php } ?>	
							</div>
							<div class="modal-footer text-center">
								<input type="submit" name="addCreditButton" id="addCreditButton" value="Confirm" class="btn btn-primary">
								<!--button type="button" id="addCreditButton" class="btn btn-primary"  >
									Confirm
								</button-->
								<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" >
									Cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
					<!-- modal-->
				 <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Remove balance</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form name="removeCreditform" class="form-horizontal" id="removeCreditform" method="post" action="" >
										<div class="form-group">
											<label class="col-sm-4 control-label"><b>Current Balance</b></label>
											<div class="col-sm-6">
												<p class="form-control-static"><b><?php echo $arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode'];?></b></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Remove Amount</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control amount" maxlength="20" placeholder="Enter amount" name="removeAmount" id="removeAmount" value="" autocomplete="off" value="" />
											</div>
											<div class="col-sm-2 col-xs-3">
												<p class="form-control-static">GBP</p>
											</div>
										</div>										
										<!--div class="form-group">
											<label class="col-sm-4 control-label">Mode</label>
											<div class="col-sm-12 col-xs-12">
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> Web top-up
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> E-Voucher
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> Physical Voucher
												</label>
											</div>
											</div>
										</div-->
										<div class="form-group m-t-20">
											<label class="col-sm-4 control-label">Comments</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" name="removeComments" id="removeComments" maxlength="40" autocomplete="off" placeholder="Enter comments" value="" />
											</div>
										</div>
									
								</div>
								<div class="row m-t-20">
									<?php if(isset($arrAddRemoveCreditInfo[0]['CreatedDate']) && $arrAddRemoveCreditInfo[0]['CreatedDate']!=''){ ?> 
									<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
										<header>
											<h2>Log Reference</h2>
						
										</header>				
										<!-- widget div-->
										<div>					
											<!-- widget edit box -->									
											<!-- end widget edit box -->					
											<!-- widget content -->
												<div class="widget-body no-padding">
												<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
														<tr>
															<!--th data-class="expand">S.No</th-->
															<th data-class="expand">Date</th>
															<th data-hide="phone,tablet">Amount Remove</th>															
															<th data-hide="phone,tablet">Comments</th>															
															
														</tr>
													</thead>
													<tbody>
													<?php $i=1; foreach($arrAddRemoveCreditInfo as $arrResult){  ?>	
															<?php if($arrResult['ActionType']=="R"){ ?>
														<tr>
															<!--td><?php //echo $i; ?></td-->
															<td><?php echo $arrResult['CreatedDate']; ?></td>
															<td><?php echo $arrResult['Amount']; ?></td>
															
															<td><?php echo $arrResult['Comments']; ?></td>
														</tr>
															<?php } ?>
													<?php $i++; } ?>			
													</tbody>
												</table>
											</div>
										</div>
									</div>	
									<?php } ?>	
								</div>									
							</div>
							<div class="modal-footer text-center">
								<input type="submit" name="removeCreditButton" id="removeCreditButton" value="Confirm" class="btn btn-primary">
								<!--button type="button" class="btn btn-primary"  >
									Confirm
								</button-->
								<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" >
									Cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	<!-- modal-->
				<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Edit Bundles details</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form name="removeCreditform" class="form-horizontal" id="removeCreditform" method="post" action="" >
										<div class="form-group">
											<label class="col-sm-4 control-label"><b>Current Balance</b></label>
											<div class="col-sm-6">
												<p class="form-control-static"><b><?php echo $arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode'];?></b></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Remove Amount</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control amount" maxlength="20" placeholder="Enter amount" name="removeAmount" id="removeAmount" value="" autocomplete="off" value="" />
											</div>
											<div class="col-sm-2 col-xs-3">
												<p class="form-control-static">GBP</p>
											</div>
										</div>										
										<!--div class="form-group">
											<label class="col-sm-4 control-label">Mode</label>
											<div class="col-sm-12 col-xs-12">
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> Web top-up
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> E-Voucher
												</label>
											</div>
											<div class="col-sm-4 col-xs-12 radio">
												<label>
												<input type="radio" class="radio" /> Physical Voucher
												</label>
											</div>
											</div>
										</div-->
										<div class="form-group m-t-20">
											<label class="col-sm-4 control-label">Comments</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" name="removeComments" id="removeComments" maxlength="40" autocomplete="off" placeholder="Enter comments" value="" />
											</div>
										</div>
									
								</div>
								<div class="row m-t-20">
									<?php if(isset($arrAddRemoveCreditInfo[0]['CreatedDate']) && $arrAddRemoveCreditInfo[0]['CreatedDate']!=''){ ?> 
									<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
										<header>
											<h2>Log Reference</h2>
						
										</header>				
										<!-- widget div-->
										<div>					
											<!-- widget edit box -->									
											<!-- end widget edit box -->					
											<!-- widget content -->
												<div class="widget-body no-padding">
												<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
														<tr>
															<!--th data-class="expand">S.No</th-->
															<th data-class="expand">Date</th>
															<th data-hide="phone,tablet">Amount Remove</th>															
															<th data-hide="phone,tablet">Comments</th>															
															
														</tr>
													</thead>
													<tbody>
													<?php $i=1; foreach($arrAddRemoveCreditInfo as $arrResult){  ?>	
															<?php if($arrResult['ActionType']=="R"){ ?>
														<tr>
															<!--td><?php //echo $i; ?></td-->
															<td><?php echo $arrResult['CreatedDate']; ?></td>
															<td><?php echo $arrResult['Amount']; ?></td>
															
															<td><?php echo $arrResult['Comments']; ?></td>
														</tr>
															<?php } ?>
													<?php $i++; } ?>			
													</tbody>
												</table>
											</div>
										</div>
									</div>	
									<?php } ?>	
								</div>									
							</div>
							<div class="modal-footer text-center">
								<input type="submit" name="removeCreditButton" id="removeCreditButton" value="Confirm" class="btn btn-primary">
								<!--button type="button" class="btn btn-primary"  >
									Confirm
								</button-->
								<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" >
									Cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	
	
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Edit Bundles details</h4>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label"><b>Current Balance</b></label>
								<div class="col-sm-6">
									<p class="form-control-static"><b><?php echo $arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode'];?></b></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Amount</label>
								<div class="col-sm-6 col-xs-9">
									<input type="text" class="form-control amount" maxlength="20" placeholder="Enter amount" name="removeAmount" id="removeAmount" value="" autocomplete="off" value="" />
							</div>
								<div class="col-sm-2 col-xs-3">
									<p class="form-control-static">GBP</p>
								</div>
							</div>	
							
							<div class="form-group">
							<label class="col-sm-4  line-height32 col-md-3 text-right">Status &nbsp;<span class="pull-right line-height32">:</span></label>
								<div class="col-sm-5">
									<select class="form-control" name="status" id="status">
										<option value="">Select</option>										
										<option value="1">Active</option>
										<option value="2">Inactive</option>																				
										
									</select>
								</div>
							</div>	
							
							
								<div class="form-group">
								<label class="col-sm-4  line-height32 col-md-3 text-right">ExpiryDate &nbsp;<span class="pull-right line-height32">:</span></label>
									<div class="input-group">
										  <span class="input-group-addon" id="basic-addon2"><i class="icon-append fa fa-calendar"></i></span>
										  <input type="text" class="form-control" name="expiryDate" id="expiryDate" placeholder="Start date" aria-describedby="basic-addon2" autocomplete="off" value="">
									</div>
							   </div>

							
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">New Threshold Limit has been updated successfully</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									Close
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	<!-- modal end-->	
	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<script>
	$(document).ready(function (){
		
		$("#getTariffInfoform").validate({			
			rules: {
				siteCode: "required",
				mobileNo: {
					required: true,
					number: true	
				},				
			},
			message: {
				siteCode: "Please select a sitecode",
				mobileNo: {
					required:"Please enter a mobile number",
					number: "Please enter only digits"
				},				
			}			
		});
		
		$("#siteCode").blur(function(){
			$("#siteCode").valid();
		});	
		
		$("#mobileNo").blur(function(){
			$("#mobileNo").valid();
		});		

		 $('.amount').keypress(function(event) {
            if(event.which == 8 || event.which == 0){
                return true;
            }
            if(event.which < 46 || event.which > 59) {
                return false;
                //event.preventDefault();
            } // prevent if not number/dot

            if(event.which == 46 && $(this).val().indexOf('.') != -1) {
                return false;
                //event.preventDefault();
            } // prevent if already dot
			
			if ($(this).val().indexOf('.') > 0) {
				var len = $(this).val().length;
				var charAfterdot = (len + 1) - $(this).val().indexOf('.');
				if (charAfterdot > 3) {
					return false;
				}
			}
			
        });
		
		
		$("input[name$='modeValue']").click(function() {
			var modeval = $(this).val();
			if(modeval==1){
				$("#paymentReferDiv").css("display", "block");
			}
			else if(modeval==2 || modeval==3){
				$("#voucherDiv").css("display", "block");				
			}else{
				$("#paymentReferDiv").css("display", "none");
				$("#voucherDiv").css("display", "none");				
			}			
		});
		
				
		$("#addCreditform").validate({	
			submitHandler : function(form){
						
						$('#myModal1').modal('toggle');
						//mobileNo
						var siteCode = $('#siteCode').val();
						var mobileNo = $.trim($('#mobileNo').val());
						var addAmount = $.trim($('#addAmount').val());		
						var modeval = $("input[name$='modeValue']").val();
						var paymentRefer = $.trim($('#paymentRefer').val());						
						var vnumber = $.trim($('#vnumber').val());
						var comments = $.trim($('#comments').val());
						var currency = $('#currency').val();
						var action = 'A';
						var manualType = '1';
						
						  if(addAmount!=''){
							  //alert(addAmount);
							  var dataString = 'siteCode='+siteCode+'&mobileNo='+mobileNo+'&addAmount='+addAmount+'&paymentmode='+modeval+'&paymentRefer='+paymentRefer+'&vnumber='+vnumber+'&comments='+comments+'&currency='+currency+'&action='+action+'&manualType='+manualType;		
							  $.ajax({
								   type: "POST",
								   url: "<?php echo base_url(); ?>tariff/updateAddBalance",
								   data: dataString,
								   //dataType: 'json',
								   success: function(data){ 
										//alert(data);
										if(data!==""){
											//window.location.reload(true);
											$("#balanceDiv").html(data);
										  swal({   
											title: 'Confirmation',   
											html:  '<span style="color:black"><b>Amount added successfully !!!</b></span>' 
										});	
										$("#addAmount").val('');
										$("#modeval").val('');
										$("#paymentRefer").val('');
										$("#vnumber").val('');
										$("#comments").val('');	
										//window.location.reload();
										  
										}else{											
											swal({   
												title: 'Warning',   
												html:  '<span style="color:black"><b>Please try again later</b></span>'
											});	
											$("#addAmount").val('');
											$("#modeval").val('');
											$("#paymentRefer").val('');
											$("#vnumber").val('');
											$("#comments").val('');	
											//location.reload(true);
										}							
								   }
								 }); 			  			  
						  }
					 
				},
			rules: {				
				addAmount: {
					required: true,
					number: true	
				},
				modeValue: "required",
				paymentRefer: "required",
				vnumber: {
					required: true,
					number: true	
				},
				comments: "required",	
			},
			message: {				
				addAmount: {
					required:"Please enter a amount",
					number: "Please enter only digits"
				},
				modeValue: "Please select a payment mode",
				paymentRefer: "Please enter a payment reference",
				vnumber: {
					required:"Please enter a Voucher number",
					number: "Please enter only digits"
				},
				comments: "Please enter a comments",	
			}			
		});
		
		
		$("#addAmount").blur(function(){
			$("#addAmount").valid();
		});	
		
		$("#modeval").blur(function(){
			$("#modeval").valid();
		});	
		
		$("#paymentRefer").blur(function(){
			$("#paymentRefer").valid();
		});
		
		$("#vnumber").blur(function(){
			$("#vnumber").valid();
		});	
		
		$("#comments").blur(function(){
			$("#comments").valid();
		});
		
		
		/*$("#addModel").click(function(){				
			$("#addAmount").val('');
			$("#modeval").val('');
			$("#paymentRefer").val('');
			$("#vnumber").val('');
			$("#comments").val('');			
		});	*/
		
		
		$("#removeCreditform").validate({	
			submitHandler : function(form){
						
						$('#myModal2').modal('toggle');
						//mobileNo
						var siteCode = $('#siteCode').val();
						var mobileNo = $.trim($('#mobileNo').val());
						var addAmount = $.trim($('#removeAmount').val());		
						var modeval = '4';
						var paymentRefer = '';						
						var vnumber = '';
						var comments = $.trim($('#removeComments').val());
						var currency = $('#currency').val();
						var action = 'R';
						var manualType = '2';
						
						  if(addAmount!=''){
							  //alert(addAmount);
							  var dataString = 'siteCode='+siteCode+'&mobileNo='+mobileNo+'&addAmount='+addAmount+'&paymentmode='+modeval+'&paymentRefer='+paymentRefer+'&vnumber='+vnumber+'&comments='+comments+'&currency='+currency+'&action='+action+'&manualType='+manualType;		
							  $.ajax({
								   type: "POST",
								   url: "<?php echo base_url(); ?>tariff/updateAddBalance",
								   data: dataString,
								   //dataType: 'json',
								   success: function(data){ 
										//alert(data);
										if(data!==""){
											//location.reload(true);
											//window.location.reload(true);
											//window.location.href = window.location.protocol +'//'+ window.location.host + window.location.pathname;
											//window.location = window.location.href;
											//window.location = window.location;
											//self.location.reload();
											window.location = window.location.href;
											$("#balanceDiv").html(data);
										  swal({   
											title: 'Confirmation',   
											html:  '<span style="color:black"><b>Amount remove successfully !!!</b></span>' 
										});	
										
										$('#removeAmount').val('');										
										$('#removeComments').val('');
										//location.reload(true);
										//document.location.reload(true); 
										window.location = window.location.pathname;

										}else{											
											swal({   
												title: 'Warning',   
												html:  '<span style="color:black"><b>Please try again later</b></span>'
											});
											$('#removeAmount').val('');										
											$('#removeComments').val('');		
										}							
								   }
								 }); 			  			  
						  }
					 
				},
			rules: {				
				removeAmount: {
					required: true,
					number: true	
				},
				/*modeval: "required",
				paymentRefer: "required",
				vnumber: {
					required: true,
					number: true	
				},*/
				removeComments: "required",	
			},
			message: {				
				removeAmount: {
					required:"Please enter a amount",
					number: "Please enter only digits"
				},
				/*modeval: "Please select a payment mode",
				paymentRefer: "Please enter a payment reference",
				vnumber: {
					required:"Please enter a Voucher number",
					number: "Please enter only digits"
				},*/
				removeComments: "Please enter a comments",	
			}			
		});
		
		
		$("#removeAmount").blur(function(){
			$("#removeAmount").valid();
		});	
		
		$("#removeComments").blur(function(){
			$("#removeComments").valid();
		});	
		
		
		/*$("#removeModel").click(function(){			
			alert('hai');
			alert($("#removeAmount").val());
			$("#removeAmount").val('');
			$("#removeComments").val('');			
		});	*/
		
		
		
		
		$('#startdate').datepicker({
				dateFormat : 'dd-mm-yy',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#finishdate').datepicker('option', 'minDate', selectedDate);
					$("#startdate").valid();
				}
			});
		
	});
	
	
	
	function restrictSpace(getId) {
		$('#'+getId).keydown(function (e) {	
			if (e.ctrlKey || e.altKey) {
			e.preventDefault();
			} else {
			var key = e.keyCode;
			//alert(key);
			if (key == 32) {
			e.preventDefault();
			}
			}
			});
	}

	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		//alert(charCode);
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	
	function addModelFunction(){
		//alert('hai');
		//alert($("#removeAmount").val());
		$("#addAmount").val('');
		$("#modeval").val('');
		$("#paymentRefer").val('');
		$("#vnumber").val('');
		$("#comments").val('');	
	}
	
	function removeModelFunction(){
		//alert('hai');
		//alert($("#removeAmount").val());
		$("#removeAmount").val('');
		$("#removeComments").val('');
	}
	
	/*function validateFloatKeyPress(el, evt) {	
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : event.keyCode;	
	alert(charCode);
    var number = el.value.split('.');
    if (charCode != 46 && charCode == 8 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

//thanks: http://javascript.nwbox.com/cursor_position/
function getSelectionStart(o) {
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', o.value.length)
		if (r.text == '') return o.value.length
		return o.value.lastIndexOf(r.text)
	} else return o.selectionStart
}*/
	
	
</script>