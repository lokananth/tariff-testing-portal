<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('session');
		$this->load->view('header_view');				
		$this->load->view('login_view');
		$this->load->view('footer_view');
	}
	
	public function checkUserLogin(){
		//echo 'hai';exit;
		//echo '<pre>';print_r($_REQUEST);exit;
		$this->load->library('session');
		
		$varUsername = trim($_REQUEST['username']);
		$varPassword = trim($_REQUEST['password']);
		
		$params = array('UserName'=>$varUsername,'Password'=>$varPassword);
		$arrLoginInfo = ApiPostHeader($this->config->item('UserLogin'), $params);
		//echo '<pre>';print_r($params);print_r($arrLoginInfo);var_dump($arrLoginInfo['errcode']);exit;				
		if(trim($arrLoginInfo['errcode'])=='0' && trim($arrLoginInfo['errcode'])!=''){			
			session_start();
			$_SESSION['userId'] = $arrLoginInfo['UserID'];
			$_SESSION['userName'] = $varUsername;
			//$_SESSION['userEmail'] = $arrLoginInfo['useremail'];			
			//$_SESSION['userType'] = $arrLoginInfo['usertype'];
			redirect('tariff');			
		}else{
			$this->session->set_flashdata('errmsg','Please enter a valid username & password');
			redirect('login');
		}		
	}
	
	public function checkAPI(){
		
		//$params = array('Mobileno'=>'32466615980');
		/*$arrLoginInfo = ApiPostHeader($this->config->item('GetSiteCodeInfo'), '');
		echo '<pre>';print_r($arrLoginInfo);exit;	*/
		
		/*$params = array('Mobileno'=>'32466615980');
		$arrLoginInfo = ApiPostHeader($this->config->item('getAddRemoveBalanceLogInfo'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	*/
		
		/*$params = array('sitecode'=>'MBE','mobileno'=>'32466615980','brand'=>'1');
		$arrLoginInfo = ApiPostHeader($this->config->item('getMainBalanceInfo'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	*/
		
		/*$params = array('sitecode'=>'MBE','mobileno'=>'32466615980','brand'=>'1');
		$arrLoginInfo = ApiPostHeader($this->config->item('getBundlesInfo'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	*/
		
		/*$params = array('sitecode'=>'MBE');
		$arrLoginInfo = ApiPostHeader($this->config->item('getAllTariffClass'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	*/
		
		/*$params = array('sitecode'=>'MBE','brand'=>'1','mobileno'=>'32466615980','tariff'=>'BA1D','Userid'=>'1');
		$arrLoginInfo = ApiPostHeader($this->config->item('updateTariffClass'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	*/
		
		$params = array('Mobileno'=>'32466615980');
		$arrLoginInfo = ApiPostHeader($this->config->item('getAddRemoveCreditBalanceLog'), $params);
		echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;	
		
	}
	
	
	public function UsersignOut(){
		session_start();
		session_destroy();
		session_unset();
		unset($_SESSION['userName']);
		redirect('login');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */