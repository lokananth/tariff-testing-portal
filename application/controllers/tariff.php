<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tariff extends CI_Controller {	
	 
	function __Construct(){
		parent::__Construct ();
		session_start();
		//echo '<pre>';print_r($_SESSION);exit;
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{
		//common array for Payment Mode
		$arrPaymentMode = array('1'=>'Web top-up','2'=>'E-Voucher','3'=>'Physical Voucher','4'=>'Manual');
		
		$data = array();
		//echo '<pre>';print_r($_REQUEST);exit;
		if(isset($_REQUEST['mobileNo']) && $_REQUEST['mobileNo']!=''){						
			$varSiteCode = trim($_REQUEST['siteCode']);			
			$varMobileNo = strip_tags(addslashes(trim($_REQUEST['mobileNo'])));		
			
			/*if($_REQUEST['siteCode']!=''){
				$varSiteCode = trim($_REQUEST['siteCode']);			
			}else{
				$varSiteCode = $_SESSION['SiteCode'];
			}
			
			if($_REQUEST['mobileNo']!=''){
				$varMobileNo = strip_tags(addslashes(trim($_REQUEST['mobileNo'])));			
			}else{
				$varMobileNo = $_SESSION['MobileNo'];
			}
						
			$_SESSION['SiteCode'] = $varSiteCode;
			$_SESSION['MobileNo'] = $varMobileNo;	*/
			
			//Get Main Balance Information	
			$params = array('sitecode'=>$varSiteCode,'mobileno'=>$varMobileNo,'brand'=>'1');
			$arrMainBalanceInfoRes = ApiPostHeader($this->config->item('getMainBalanceInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrMainBalanceInfoRes);exit;
			
			if(count($arrMainBalanceInfoRes)>0 && $arrMainBalanceInfoRes['errcode']=='0'){			
				$data['arrMainBalanceInfo'] = $arrMainBalanceInfoRes;		
			}else{
				$data['arrMainBalanceInfo'] = array();
			}
			
			//Get All Tariff Class for Selected Sitecode
			$params = array('sitecode'=>$varSiteCode);
			$arrTariffClassListInfoRes = ApiPostHeader($this->config->item('getAllTariffClass'), $params);
			//echo '<pre>';print_r($params);print_r($arrTariffClassListInfoRes);exit;	
			
			if(count($arrTariffClassListInfoRes)>0 && $arrTariffClassListInfoRes[0]['errcode']=='0'){			
				$data['arrTariffClassListInfo'] = $arrTariffClassListInfoRes;		
			}else{
				$data['arrTariffClassListInfo'] = array();
			}
			
			
			//Get Bundles Information	
			$params = array('sitecode'=>$varSiteCode,'mobileno'=>$varMobileNo,'brand'=>'1');
			$arrBundlesInfoRes = ApiPostHeader($this->config->item('getBundlesInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrBundlesInfoRes);exit;
			
			if(count($arrBundlesInfoRes)>0 && $arrBundlesInfoRes[0]['errcode']=='0'){			
				$data['arrBundlesInfo'] = $arrBundlesInfoRes;		
			}else{
				$data['arrBundlesInfo'] = array();
			}
			
			//Get Add Remove Credit Balance Log Information
			$params = array('Mobileno'=>$varMobileNo);
			$arrAddRemoveCreditInfoRes = ApiPostHeader($this->config->item('getAddRemoveCreditBalanceLog'), $params);
			//echo '<pre>';print_r($params);print_r($arrAddRemoveCreditInfoRes);exit;	
			
			if(count($arrAddRemoveCreditInfoRes)>0 && $arrAddRemoveCreditInfoRes[0]['errcode']=='0'){			
				$data['arrAddRemoveCreditInfo'] = $arrAddRemoveCreditInfoRes;		
			}else{
				$data['arrAddRemoveCreditInfo'] = array();
			}
			
			$data['varSiteCode'] = $varSiteCode;
			$data['varMobileNo'] = $varMobileNo;
			$data['arrPaymentMode'] = $arrPaymentMode;
		}		
		
		
		
		$arrSiteCodeInfoRes = ApiPostHeader($this->config->item('GetSiteCodeInfo'), '');		
		//echo '<pre>';print_r($arrSiteCodeInfoRes);exit;
		
		if(count($arrSiteCodeInfoRes)>0 && $arrSiteCodeInfoRes[0]['errcode']=='0'){			
				$data['arrSiteCodeInfo'] = $arrSiteCodeInfoRes;		
		}else{
			$data['arrSiteCodeInfo'] = array();
		}
		
		
		//if($_REQUEST[''])
	
		$this->load->view('header_view');
		$this->load->view('innerMenu_view');		
		$this->load->view('leftMenu_view');
		$this->load->view('tariff_view',$data);
		$this->load->view('footer_view');
	}
	
	public function updateTariffClass(){
		$varSiteCode =  $this->input->post('siteCode');
		$varMobileNo =  $this->input->post('mobileNo');
		$varTariffClass =  $this->input->post('selectTariffClass');	
		
		if($varTariffClass!=''){
			$params = array('sitecode'=>$varSiteCode,'brand'=>'1','mobileno'=>$varMobileNo,'tariff'=>$varTariffClass,'Userid'=>$_SESSION['userId']);
			$arrUpdateTariffClass = ApiPostHeader($this->config->item('updateTariffClass'), $params);
			//echo '<pre>';print_r($params);print_r($arrUpdateTariffClass);exit;				
			if($arrUpdateTariffClass['errcode']=='0'){
				echo $arrUpdateTariffClass['errcode'];exit;
			}else{
				echo '';
			}	
		}else{
			echo '';
		}
	}
	
	public function updateAddBalance(){
		//echo '<pre>';print_r($_REQUEST);exit;
		$varSiteCode =  $this->input->post('siteCode');		
		$varMobileNo =  $this->input->post('mobileNo');
		$varAmount =  $this->input->post('addAmount');
		$varPaymentMode =  $this->input->post('paymentmode');
		$varPaymentReference =  $this->input->post('paymentRefer');
		$varVoucherNumber =  $this->input->post('vnumber');
		$varComments =  $this->input->post('comments');
		$varCurrency =  $this->input->post('currency');
		$varAction =  $this->input->post('action');
		$varManualType =  $this->input->post('manualType');
		
		if($varAmount!=''){
			$params = array('sitecode'=>$varSiteCode,'mobileno'=>$varMobileNo,'brand'=>'1','Amount'=>$varAmount,'Manual_Balance_Type'=>$varManualType,'PayMode'=>$varPaymentMode,'VoucherNo'=>$varVoucherNumber,'Comments'=>$varComments,'PaymentRef'=>$varPaymentReference,'Currency'=>$varCurrency,'User_Id'=>$_SESSION['userId'],'Action_Type'=>$varAction);
			$arrAddCreditInfo = ApiPostHeader($this->config->item('updateAddRemoveCreditBalance'), $params);
			//echo '<pre>';print_r($params);print_r($arrAddCreditInfo);exit;	
			if($arrAddCreditInfo['errcode']=='0'){
				//echo $arrAddCreditInfo['errcode'];exit;
				
				//Get Main Balance Information	
				$params = array('sitecode'=>$varSiteCode,'mobileno'=>$varMobileNo,'brand'=>'1');
				$arrMainBalanceInfo = ApiPostHeader($this->config->item('getMainBalanceInfo'), $params);
				$varResult='';
				
				$varResult .='<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Product</th>
													<th data-hide="phone">Current Balance</th>
													<th>Action</th>													
												</tr>
											</thead>
											<tbody>												
												<tr>';
												
						$varResult .="<td>".$arrMainBalanceInfo['product_type']."</td>
													<td>".$arrMainBalanceInfo['current_bal'].' '.$arrMainBalanceInfo['currcode']."</td>
													<td>";
													
						$varResult .=			'<a href="#" data-toggle="modal" class="btn btn-primary" data-target="#myModal1">Add</a> 
														<a href="#" data-toggle="modal" class="btn btn-danger" data-target="#myModal2">Remove</a>
													</td>
												</tr>											
																							
											</tbody>
										</table>';
				echo $varResult;exit;
				
			}else{
				echo '';				
			}
		}else{
			echo '';			
		}		
	}
	
	public function searchByCustomerList(){
		echo '<pre>';print_r($_REQUEST);exit;
		
	}

}
?>